package fr.dawan.exemplejunit;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import exemplejunit.Calcul;

@DisplayName("Mon Premier test")
// @Disabled
public class PremierTest {

    @BeforeAll
    public static void setup() {
        System.out.println("Before All");
    }

    @BeforeEach
    public void init() {
        System.out.println("Before Each");
    }

    @AfterEach
    public void clean() {
        System.out.println("After Each");
    }

    @Test
    @DisplayName("Un test Simple")
    public void simpleTest() {
        System.out.println("Un test simple");
        assertEquals(1 + 1, 2);
    }

    @Test
    // @Disabled
    public void secondTest() {
        System.out.println("Un deuxième test simple");
        assertEquals(1 + 1, 2);
    }

    @Test
    public void testSomme() {
        int res = Calcul.somme(3, 4);
        assertEquals(7, res);
        assertNotEquals(6, res);
    }

    @Test
    public void testSame() {
        String s1 = new String("hello");
        String s11 = s1;
        String s2 = "hello";
        String s3 = "hello";
        assertSame(s1, s11);
        assertNotSame(s1, s3);
        assertSame(s2, s3);
    }

    @Test
    public void testEquals() {
        String s1 = "hello";
        String s2 = new String("hello");
        String s3 = "world";
        assertEquals(s1, s2);
        assertNotEquals(s1, s3);
    }

    @Test
    public void testNull() {
        String a = null;
        String b = "azerty";
        assertNull(a);
        assertNotNull(b);
    }

    @Test
    public void testThrow() {
        assertThrows(FileNotFoundException.class, () -> {
            FileReader f = new FileReader("nexistepas");
        });
        assertThrows(Exception.class, () -> {
            FileReader f = new FileReader("nexistepas");
        });
    }

    @Test
    public void testArrayEquals() {
        double t1[] = { 1.5, 5.6, 0.2 };
        double t2[] = { 1.5, 5.6, 0.200000001 };
        assertArrayEquals(t1, t2, 0.0000001);
        double t3[] = { 1.5, 5.6 };
        // assertArrayEquals(t1, t3, 0.0000001);
    }

    @Test
    public void testIterable() {
        List<Integer> lstA = Arrays.asList(1, 6, 4, 5, 8, 7);
        List<Integer> lstB = Arrays.asList(1, 6, 4, 5, 8, 7);
        assertIterableEquals(lstA, lstB);
    }

    @Test
    public void testTimeout() {
        assertTimeout(Duration.ofMillis(220), () -> {
            Calcul.traitement();
        });
        assertTimeoutPreemptively(Duration.ofMillis(220), () -> {
            Calcul.traitement();
        });
    }

//    @Test
//    public void testFail() {
//        fail();
//    }

    @Test
    public void testRegroupementAssertion() {
        Dimension sut = new Dimension(800, 600);
        assertAll("Dimensions non conformes", () -> assertEquals(800, sut.getWidth()),
                () -> assertEquals(600, sut.getHeight()));

    }

    @AfterAll
    public static void tearDown() {
        System.out.println("After All");
    }
}
