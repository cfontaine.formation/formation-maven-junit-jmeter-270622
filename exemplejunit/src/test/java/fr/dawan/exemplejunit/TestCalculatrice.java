package fr.dawan.exemplejunit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import exemplejunit.Calculatrice;

public class TestCalculatrice {
    @Test
    public void testMultiplication() throws Exception {
        assertEquals(6.0,Calculatrice.calculer(2.0, "*", 3.0));
    }
    
    @Test
    public void testAddition() throws Exception {
        assertEquals(3.0,Calculatrice.calculer(1.0, "+", 2.0));
    }
    
    @Test
    public void testSoustraction() throws Exception {
        assertEquals(1.0,Calculatrice.calculer(3.0, "-", 2.0));
    }
    
    @Test
    public void testDivision() throws Exception {
        assertEquals(0.5,Calculatrice.calculer(2.0, "/", 4.0));
    }
    
    @Test
    @DisplayName("Tester la division par 0")
    public void testDivPar0() {
        assertThrows(ArithmeticException.class,()->{Calculatrice.calculer(5.5, "/", 0.0);});
    }
    
    @Test
    @DisplayName("Opérateur inconnue")
    public void operateurIncorrecte() {
        assertThrows(Exception.class,()->{Calculatrice.calculer(5.5, "%", 0.0);});
    }
}
