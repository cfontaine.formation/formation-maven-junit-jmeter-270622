package exemplejunit;

public class Calculatrice {


    public static double calculer(double v1, String op, double v2) throws Exception {
        switch (op) {
        case "+":
            return v1 + v2;
        case "-":
            return v1 - v2;
        case "*":
            return v1 * v2;
        case "/":
            if (v2 == 0.0) { // (v2>-0.000000001) && (v2<0.000000001)
                throw new ArithmeticException("Division par 0");
            } else {
                return v1 / v2;
            }
        default:
            throw new Exception(op + " n'est pas un opérateur valide");
        }

    }
}
