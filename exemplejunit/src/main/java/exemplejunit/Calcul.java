package exemplejunit;

public class Calcul {
    
    public static int somme(int a,int b) {
        return a+b;
    }
    
    public static double multiplication(double v1, double v2) {
        return v1*v2;
    }
    
    public static void traitement() throws InterruptedException {
        Thread.sleep(200);
    }
}
