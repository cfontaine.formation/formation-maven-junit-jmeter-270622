package exercice.modele;

public class Film {

    private long id;
    
    private String titre;
    
    private boolean vu;
    
    private int note;

    public Film() {

    }

    public Film(long id, String titre, boolean vu, int note) {
        this.id = id;
        this.titre = titre;
        this.vu = vu;
        this.note = note;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public boolean isVu() {
        return vu;
    }

    public void setVu(boolean vu) {
        this.vu = vu;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Film [id=" + id + ", titre=" + titre + ", vu=" + vu + ", note=" + note + "]";
    }
    
    
}
