package exercice.dao;

import java.util.ArrayList;
import java.util.List;

import exercice.modele.Film;

public class FakeFilmDao {
    private static List<Film> films=new ArrayList<Film>();
    
    private static int nextId=4;
    
    static {
        Film f=new Film(1,"Avenger",false,0);
        films.add(f);
        f=new Film(2,"Je suis une legende",false,0);
        films.add(f);
        f=new Film(3,"Dragonball evolution",false,0);
        films.add(f);
    }
    
    public void saveOrUpdate(Film film){
        if(film.getId()==0) {
            films.add(film);
            film.setId(nextId++);
        }
        else {
            for(Film f :films) {
                if(f.getId()==film.getId()) {
                    f.setTitre(film.getTitre());
                    f.setNote(film.getNote());
                    f.setVu(film.isVu());
                }
            }
        }
    }
    
    public void delete(long id) {
        for(Film f :films) {
            if(f.getId()==id) {
                films.remove(f);
            }
        }
    }
    
    public void delete(Film film) {
        films.remove(film);
    }
    
    public Film findById(long id) {
       for(Film f :films) {
           if(f.getId()==id) {
               return f;
           }  
       }
       return null;
    }
    
    public List<Film> findAll(){
        return films;
    }
    
}
