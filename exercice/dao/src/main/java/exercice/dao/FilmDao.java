package exercice.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import exercice.modele.Film;

public class FilmDao {

    public void saveOrUpdate(Film film) {
        try (Connection cnx = createConnection()) {
            PreparedStatement ps = cnx.prepareStatement("INSERT INTO films(id,titre,vue,note) VALUES(?,?,?,?)");
            ps.setLong(1, film.getId());
            ps.setString(2, film.getTitre());
            ps.setBoolean(3, film.isVu());
            ps.setInt(4, film.getNote());
            ps.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(long id) {
        try (Connection cnx = createConnection()) {
            PreparedStatement ps = cnx.prepareStatement("DELETE FROM films WHERE id=?");
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } 
    }

    public void delete(Film film) {
        delete(film.getId());
    }

    public Film findById(long id) {
        Film film = null;
        try (Connection cnx = createConnection()) {
            PreparedStatement ps = cnx.prepareStatement("SELECT * FROM films WHERE id=?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                film = new Film(id, rs.getString("titre"), rs.getBoolean("vue"), rs.getInt("note"));
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return film;
    }

    public List<Film> findAll() {
        List<Film> films = new ArrayList<Film>();
        try (Connection cnx = createConnection()) {
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM films");
            while (rs.next()) {
                Film f = new Film(rs.getLong("id"), rs.getString("titre"), rs.getBoolean("vue"), rs.getInt("note"));
                films.add(f);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return films;
    }

    private Connection createConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/film", "root", "");
    }
}
